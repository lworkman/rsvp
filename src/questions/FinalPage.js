import React from "react";
import { FormInput } from "../components/FormInput";
import { FadeIn } from "../components/FadeIn";

export const FinalPage = ({ values, handleChange }) => (
  <FadeIn>
    <FormInput label="You get one song request" onChange={handleChange} value={values.songRequest} name="songRequest" />
  </FadeIn>)
