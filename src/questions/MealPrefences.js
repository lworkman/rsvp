import { FieldArray } from "formik";
import React from "react";
import { FormInput } from "../components/FormInput";
import { FormRadioGroup } from "../components/FormRadioGroup";
import { FadeIn } from "../components/FadeIn";

export const MealPreferences = ({ handleChange, values }) => (
  <FadeIn>
    <FieldArray
      name="guests"
      render={arrayHelpers => (
        <div>
          {values.guests.map((guest, index) => (
            <div key={index}>
              <span>{guest.firstName} {guest.secondName}</span>
              <FormRadioGroup
                onChange={handleChange}
                value={guest.mealPreference}
                name={`guests[${index}].mealPreference`}
                options={[
                  {
                    label: "Prime-Rib",
                    value: "meat"
                  },
                  {
                    label: "Non-meat",
                    value: "nonMeat"
                  }
                ]}
              />
              <FormInput onChange={handleChange} value={guest.dietaryRestrictions} name={`guests[${index}].dietaryRestrictions`} label="Any dietary restrictions?" />
            </div>
          ))}
        </div>
      )}
    />
  </FadeIn>
)
