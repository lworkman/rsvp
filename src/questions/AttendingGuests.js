import React from "react";
import { FieldArray } from "formik";
import { FormInput } from "../components/FormInput";
import { WeddingButton } from "../components/WeddingButton";
import { FadeIn } from "../components/FadeIn";

export const AttendingGuests = ({ handleChange, values, emptyGuest, guestLimit }) => (
  <FadeIn>
    <FieldArray
      name="guests"
      render={arrayHelpers => (
        <div>
          <h4>Guest Information</h4>
          {values.guests.map((guest, index) => (
            <div key={index}>
              {index !== 0 && <WeddingButton type="button" onClick={() => arrayHelpers.remove(index)}>Remove Guest</WeddingButton>}
              <FormInput onChange={handleChange} value={guest.firstName} name={`guests[${index}].firstName`} label="First Name" />
              <FormInput onChange={handleChange} value={guest.secondName} name={`guests[${index}].secondName`} label="Last Name" />
            </div>
          ))}
          <div>
            {values.guests.length <= guestLimit && <WeddingButton type="button" onClick={() => arrayHelpers.push({ ...emptyGuest })}>Add Guest</WeddingButton>}
          </div>
        </div>
      )}
    />
  </FadeIn>
)
