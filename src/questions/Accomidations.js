import { FieldArray } from "formik";
import React from "react";
import { FormCheckboxGroup } from "../components/FormCheckBoxGroup";
import { FadeIn } from "../components/FadeIn";

export const Accomidations = ({ values }) => (
  <FadeIn>
    <FieldArray
      name="accomidationPreferences"
      render={arrayHelpers => (
        <div>
          <FormCheckboxGroup
            values={values.accomidationPreferences}
            addElement={arrayHelpers.push}
            removeElement={arrayHelpers.remove}
            options={[
              {
                label: "Private Room",
                value: "private room"
              },
              {
                label: "Shared Apartment",
                value: "shared apartment"
              },
              {
                label: "Shared dorm-style",
                value: "shared dorm-style"
              }
            ]}
          />
        </div>
      )}
    />
  </FadeIn>
)
