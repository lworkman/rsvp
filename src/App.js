import { Form, Formik } from "formik";
import React, { Component } from 'react';
import './App.css';
import { EqualSpacer } from "./components/EqualSpacer";
import { WeddingButton } from './components/WeddingButton';
import { WeddingCard } from "./components/WeddingCard";
import { FieldArray } from "formik";
import { FadeIn } from "./components/FadeIn";
import { guestList } from "./guestList";
import { addToDatabase } from "./firebaseClient";
import { Spinner } from "./components/Spinner";
import * as Yup from "yup";
import { FormCheckboxGroup } from "./components/FormCheckBoxGroup";
import { FormInput } from "./components/FormInput";
import { FormRadioGroup } from "./components/FormRadioGroup";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { Radio } from "@material-ui/core";



class App extends Component {

  emptyGuest = {
    name: "",
    mealPreference: "",
    dietaryRestrictions: "",
    hasDietaryRestriction: false
  };
  pageCount = 4;

  yupSchema = Yup.object().shape({
    songRequest: Yup.string(),
    guests: Yup.array(Yup.object().shape({
      name: Yup.string().required('Please enter your name'),
      mealPreference: Yup.string().required('Please enter your name'),
      dietaryRestrictions: Yup.string(),
      hasDietaryRestriction: Yup.boolean()
    })).required(),
    comment: Yup.string(),
    accomidationPreferences: Yup.array(Yup.string()).required("Please choose at least one preference")
  })

  theme = createMuiTheme({
    palette: {
      primary: { main: "#687f46" }, // Purple and green play nicely together.
      secondary: { main: "#3c533a" }, // This is just green.A700 as hex.
    }
  });

  constructor(props) {
    super(props);

    const initialState = {
      isAttending: null,
      hasSubmited: false,
      guestLimit: 1,
      page: 0,
      initialValues: {
        accomidationPreferences: [
          "private room",
          "shared apartment",
          "shared dorm-style"
        ],
        guests: [{
          name: "",
          mealPreference: "",
          dietaryRestrictions: "",
          hasDietaryRestriction: false
        }],
        songRequest: "",
        comment: ""
      },
      guestPreferences: null,
      submitting: false,
      comment: "",
      dirtySubmit: false
    };

    if (window.location.href.indexOf("?id=") !== -1) {
      const id = /\?id=(.*)/.exec(window.location.href)[1];
      const guestPreferences = guestList[id];
      if (guestPreferences) {
        initialState.guestPreferences = guestPreferences;
        initialState.guestLimit = guestPreferences.limit;
        initialState.initialValues.guests[0].name = guestPreferences.name;
      }
    }

    this.state = initialState;
  }

  setAttendingState = (isAttending) => this.setState({ isAttending })

  nextPage = () => this.setState({ page: this.state.page + 1 });

  previousPage = () => this.setState({ page: this.state.page - 1 });

  notComing = () => {
    const { guestPreferences } = this.state;
    this.setAttendingState(false);
    if (guestPreferences) {
      addToDatabase({
        isAttending: false,
        name: guestPreferences.name,
      });
    }
  }

  formSubmit = (value) => {
    this.setState({
      submitting: true
    });
    const cohortId = Math.floor(Math.random() * 564727123).toString();
    const guestsToAdd = value.guests.map(guest => ({
      isAttending: true,
      cohortId,
      sharedApartment: value.accomidationPreferences.indexOf("private room") !== -1,
      sharedDorm: value.accomidationPreferences.indexOf("shared apartment") !== -1,
      privateRoom: value.accomidationPreferences.indexOf("shared dorm-style") !== -1,
      noAccomidation: value.accomidationPreferences.indexOf("not-attending") !== -1,
      ...guest
    }))
    guestsToAdd[0]["songRequest"] = value.songRequest;
    guestsToAdd[0]["comment"] = value.comment;

    Promise.all(guestsToAdd.map(addToDatabase)).then(() => {
      window.location = "./information.html";
    });
  }

  checkIfValid = (page, errors) => {
    switch (page) {
      case 0:
        return errors.guests && errors.guests.some(guest => guest.secondName || guest.name);
      case 1:
        return errors.guests && errors.guests.some(guest => guest.mealPreference);
      case 2:
        return errors.accomidationPreferences;
      case 3:
      default:
        return true;
    }
  }

  accomidationPreferenceChange = (setFieldValue) => {
    setFieldValue("accomidationPreferences", ["not-attending"])
  }

  submitForm = (isValid, handleSubmit) => {
    if (isValid) {
      handleSubmit();
    }
    this.setState({
      dirtySubmit: true
    });
  }

  render() {

    const { isAttending, guestLimit, initialValues, submitting, hasSubmited } = this.state;
    const name = initialValues.guests[0].name ? initialValues.guests[0].name.split(" ")[0] : "";
    let content;

    if (isAttending === null) {
      content = (
        <FadeIn fadeUp>
          <WeddingCard>
            <h3 className="styled">{name || "Hello!"}</h3>
            <p>Please join us to celebrate<br /> the marriage of</p>
            <h1 style={{ marginTop: "15px" }}>Miranda Rotz</h1>
            <h1 className="no-margin">+</h1>
            <h1 style={{ marginBottom: "20px" }}>Liam Workman</h1>
            <p>16:00 | June 29, 2019</p>
            <p>Lake Cowichan, BC</p>
            <p>Reception to follow</p>
            <h2 style={{ margin: "20px auto" }}>Will you be able to attend?</h2>
            <EqualSpacer>
              <WeddingButton primary="true" onClick={() => this.setAttendingState(true)}>Yes</WeddingButton>
              <WeddingButton onClick={this.notComing}>No</WeddingButton>
            </EqualSpacer>
          </WeddingCard>
        </FadeIn>
      )
    } else if (!isAttending) {
      content = (
        <WeddingCard>
          <h3 style={{ marginTop: 40 }}>Okay, thanks for letting us know {name}!</h3>
        </WeddingCard>
      )
    } else if (hasSubmited) {
      content = (
        <WeddingCard>
          <div>Looking forward to it!</div>
        </WeddingCard>
      );
    } else {
      content = (
        <WeddingCard>
          <Formik
            initialValues={initialValues}
            onSubmit={this.formSubmit}
            validationSchema={this.yupSchema}
          >
            {({ handleChange, values, isValid, handleSubmit, setFieldValue }) => (
              <FadeIn>
                <Form style={{ display: "flex", flexDirection: "column", flexGrow: 1 }}>
                  <h2 className="styled">RSVP</h2>
                  <div className="question-section">
                    <h3>We're excited you can make it, and we have a few questions</h3>
                    <FieldArray
                      name="guests"
                      render={arrayHelpers => (
                        <div>
                          {values.guests.map((guest, index) => (
                            <div key={index}>
                              {/* <div style={{minHeight: 22}}>{guest.name} {guest.secondName}</div> */}
                              <FormInput onChange={handleChange} value={guest.name} name={`guests[${index}].name`} label={index === 0 ? "Your Name" : "Honoured Guest"} />
                              {/* <FormInput onChange={handleChange} value={guest.secondName} name={`guests[${index}].secondName`} label="Last Name" /> */}
                            </div>
                          ))}
                          <div>
                            {values.guests.length <= guestLimit && <WeddingButton fullwidth="true" primary="true" type="button" onClick={() => arrayHelpers.push({ ...this.emptyGuest })}>Add plus one</WeddingButton>}
                            {values.guests.length > 1 && <WeddingButton fullwidth="true" primary="true" type="button" onClick={() => arrayHelpers.remove(values.guests.length - 1)}>Remove Guest</WeddingButton>}
                          </div>
                        </div>
                      )}
                    />
                  </div>
                  <div className="question-section">
                    <h3>Accommodations</h3>
                    <p style={{ fontSize: "0.9rem", textTransform: "inherit" }}>The wedding is in Lake Cowichan at an old summer camp. Expect beautiful woods and plenty of space. The camp has lots of beds spread out across a few buildings in a variety of room types.</p>
                    <p style={{ fontSize: "0.9rem", textTransform: "inherit", marginTop: "10px" }}>Overnight accommodations are $33 per guest. Please check off a couple options and we'll try our best to match you with what you select.</p>
                    <FieldArray
                      name="accomidationPreferences"
                      render={arrayHelpers => (
                        <div>
                          <FormCheckboxGroup
                            values={values.accomidationPreferences}
                            addElement={arrayHelpers.push}
                            removeElement={arrayHelpers.remove}
                            options={[
                              {
                                label: "Private Room (Limited)",
                                value: "private room"
                              },
                              {
                                label: "Shared Apartment",
                                value: "shared apartment"
                              },
                              {
                                label: "Shared dorm-style",
                                value: "shared dorm-style"
                              }
                            ]}
                          />

                          <FormControlLabel
                            control={
                              <Radio name="accomidationPreferences" onChange={() => this.accomidationPreferenceChange(setFieldValue)} checked={values.accomidationPreferences[0] === "not-attending"} />
                            }
                            label={`${values.guests.length > 1 ? "We" : "I"} won't staying over`}
                          />
                        </div>
                      )}
                    />
                  </div>
                  <div className="question-section">
                    <h3>Meal Preferences</h3>
                    <p style={{ fontSize: "0.9rem", textTransform: "inherit" }}>Dinner will be provided and there will be a breakfast in the morning for anyone staying the night.</p>
                    <FieldArray
                      name="guests"
                      render={arrayHelpers => (
                        <div>
                          {values.guests.map((guest, index) => (
                            <div key={index} style={{ margin: "10px 0" }}>
                              <div style={{ minHeight: 22 }}>{guest.name}</div>
                              <FormRadioGroup
                                onChange={handleChange}
                                value={guest.mealPreference}
                                name={`guests[${index}].mealPreference`}
                                options={[
                                  {
                                    label: "Meat",
                                    value: "meat"
                                  },
                                  {
                                    label: "Vegetarian",
                                    value: "nonMeat"
                                  }
                                ]}
                              />
                              <FormControlLabel
                                label="Dietary Restrictions?"
                                labelPlacement="end"
                                control={
                                  <Switch
                                    checked={guest.hasDietaryRestriction}
                                    onChange={handleChange}
                                    value="checked"
                                    name={`guests[${index}].hasDietaryRestriction`}
                                  />
                                }
                              />
                              {guest.hasDietaryRestriction && <FormInput onChange={handleChange} value={guest.dietaryRestrictions} name={`guests[${index}].dietaryRestrictions`} label="Dietary Restrictions" />}
                            </div>
                          ))}
                        </div>
                      )}
                    />
                  </div>
                  <div className="question-section">
                    <h3>Final Questions</h3>
                    <FormInput label="Any song requests?" onChange={handleChange} value={values.songRequest} name="songRequest" />
                  </div>
                  <div className="question-section">
                    <FormInput label="Anything else?" onChange={handleChange} value={values.comment} name="comment" />
                  </div>
                  <WeddingButton primary="true" isdisabled={!isValid} onClick={() => this.submitForm(isValid, handleSubmit)} type="submit">Submit</WeddingButton>
                  <p style={{ fontSize: "0.9rem", textTransform: "inherit", color:"#cf2525", height: "20px" }}>{this.state.dirtySubmit && !isValid && "Please check you filled all the fields"}</p>
                </Form>
              </FadeIn>
            )}
          </Formik>
        </WeddingCard>
      );
    }

    return (
      <MuiThemeProvider theme={this.theme}>
        {content}
        {/* {isAttending && <WeddingInfo />} */}
        {submitting && <Spinner />}
      </MuiThemeProvider>
    )

  }
}

export default App;
