import React from "react";
import "./WeddingButton.css";

export const WeddingButton = ({ children, ...props }) => {

  return (
    <button className={`weddingButton ${props.primary ? "primary" : ""} ${props.fullwidth ? "full-width" : ""} ${props.isdisabled ? "disabled" : ""}`} {...props}>{children}</button>
  );
}