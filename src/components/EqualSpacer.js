import React from "react";

export const EqualSpacer = ({ children }) => <div style={{ display: "flex", alignItems: "center", justifyContent: "space-around" }}>{children}</div>