import React from "react";
import { WeddingCard } from "./WeddingCard";
import { FadeIn } from "./FadeIn";

export const WeddingInfo = () => (
  <FadeIn fadeUp>
    <WeddingCard>
      <h4>Wedding Information:</h4>
      <ul>
        <li>The Wedding is in Lake Cowichan, at the Lake Cowichan Education Centre</li>
        <li>The venue is an old summer camp, so expect gorgeous sights, a private beach, beautiful woods, and plenty of space</li>
        <li>The accomidations are spread out across private rooms, shared apartments, and "dorm" style beds. There's plenty of room, and no one will have to share a room if they don't want to (unless no one wants to share a room, in which case you will have to)</li>
        <li>There are plenty of other accomidations if you don't want to stay on site. We'll also be providing a free shuttle to and from Lake Cowichan.</li>
        <li>We'll be providing arrival times and a general itenerary closer to the date</li>
        <li>Meals will be buffet style, and our caterer can provide for any dietary restrictions.</li>
      </ul>
    </WeddingCard>
  </FadeIn>
)