import React from "react";
import "./Spinner.css";
import { CircularProgress } from "@material-ui/core";

export const Spinner = () => <div className="spinner-container">
  <div className="spinner">
    <CircularProgress />
  </div>
  <div className="spinner-background" />
</div>