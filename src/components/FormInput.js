import React from "react";
import "./FormInput.css"
import TextField from "@material-ui/core/TextField";

export const FormInput = ({ value, onChange, name, label }) => {

  return (
    <div className="formInput">
      <TextField fullWidth label={label ? label : name} value={value} onChange={onChange} name={name} />
    </div>
  )
}