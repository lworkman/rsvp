import React from "react";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

export const FormCheckboxGroup = ({ values, addElement, removeElement, label, options = [] }) => {

  const emitValue = (value) => {
    if (values[0] === "not-attending") {
      removeElement(0);
    }
    if (values.indexOf(value) !== -1) {
      removeElement(values.indexOf(value));
    } else {
      addElement(value);
    }
  }

  return (
    <div>
      <span>{label}</span>
      {options.map((option, index) => (
        <div key={index}>
        <FormControlLabel
          control={
            <Checkbox type="checkbox" onChange={() => emitValue(option.value)} checked={values.indexOf(option.value) !== -1} />            
          }
          label={option.label}
        />
        </div>
      ))}
    </div>
  );
}