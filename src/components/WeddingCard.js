import React from "react";
import "./WeddingCard.css";

export const WeddingCard = ({children}) => (
  <div className="weddingCard">
    <div className="fern fern-top"/>
    <div className="wedding-card-container">
      {children}
    </div>
    <div className="fern fern-bottom"/>
  </div>
)