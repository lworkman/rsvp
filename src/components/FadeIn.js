import React from "react";
import "./FadeIn.css";

export class FadeIn extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasMounted: false
    }
  }

  componentDidMount() {
    setTimeout(() => this.setState({ hasMounted: true }), 0);
  }

  render() {
    const { children, fadeUp } = this.props;
    const { hasMounted } = this.state;

    return (
      <div className="fadeIn" style={{ opacity: hasMounted ? 1 : 0, transform: `translateY(${!hasMounted && fadeUp ? "10px" : "0px"})` }}>
        {children}
      </div>
    )
  }
}