import React from "react";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';

export const FormRadioGroup = ({ value, onChange, label, name, options = [] }) => {

  const emitValue = (value) => {
    onChange({
      target: {
        name,
        value
      }
    });
  }

  return (
    <div>
      <span>{label}</span>
      {options.map((option, index) => (
        <div key={index} style={{display: "inline-block"}}>
          <FormControlLabel
            control={<Radio type="radio" onChange={() => emitValue(option.value)} checked={option.value === value} />}
            label={option.label}
          />
        </div>
      ))}
    </div>
  );
}