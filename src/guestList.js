export const guestList = {
  "172398719823": {
    limit: 1,
    name: "Liam Workman"
  },
  "5496482": {
    limit: 1,
    name: "Nessa Kenny"
  },
  "6068564": {
    limit: 0,
    name: "Fiona Em"
  },
  "7867180": {
    limit: 0,
    name: "Cleo Laskarin"
  },
  "3979270": {
    limit: 0,
    name: "Brittany Bernard"
  },
  "1027256": {
    limit: 1,
    name: "Liz Smith"
  },
  "889728": {
    limit: 1,
    name: "Robyn Chatwin-Davies"
  },
  "5908647": {
    limit: 1,
    name: "Lyndsay Sklenka"
  },
  "1393937": {
    limit: 1,
    name: "Brooke Caza"
  },
  "3542450": {
    limit: 0,
    name: "Jameson Bosomworth"
  },
  "96992": {
    limit: 1,
    name: "Gaby Emmett"
  },
  "3253445": {
    limit: 1,
    name: "Jon Gagnon"
  },
  "8677426": {
    limit: 0,
    name: "Simon Lay"
  },
  "2283392": {
    limit: 1,
    name: "Graham Henry"
  },
  "4071736": {
    limit: 1,
    name: "Dan Harton"
  },
  "6997477": {
    limit: 0,
    name: "James Green"
  },
  "4161460": {
    limit: 0,
    name: "Michael Robinson"
  },
  "4308205": {
    limit: 1,
    name: "Brock Zawila"
  },
  "6430097": {
    limit: 1,
    name: "Josh Zapf"
  },
  "8678724": {
    limit: 0,
    name: "Ron Coombes"
  },
  "4213101": {
    limit: 1,
    name: "Jim Rotz"
  },
  "599484": {
    limit: 0,
    name: "Colin Scobie"
  },
  "2836325": {
    limit: 1,
    name: "Nick Workman"
  },
  "8731324": {
    limit: 0,
    name: "Wes MacInnis"
  },
  "5230213": {
    limit: 3,
    name: "Steph Coombes"
  },
  "843614": {
    limit: 0,
    name: "Chris Coombes"
  },
  "9778448": {
    limit: 0,
    name: "Ryan Coombes"
  },
  "523318": {
    limit: 1,
    name: "Roy Gilbert"
  },
  "6126736": {
    limit: 0,
    name: "Jordan Rotz"
  },
  "3571516": {
    limit: 2,
    name: "Zak Workman"
  },
  "1512140": {
    limit: 1,
    name: "Sheriff Debbie"
  },
  "2625851": {
    limit: 1,
    name: "Emma Russell"
  },
  "7604476": {
    limit: 1,
    name: "Liz Blaine"
  },
  "2344690": {
    limit: 1,
    name: "Erin Workman"
  },
  "7763407": {
    limit: 0,
    name: "Sam Workman"
  },
  "3361952": {
    limit: 3,
    name: "Rebecca Bradshaw"
  },
  "6549107": {
    limit: 1,
    name: "Jessica Friesen"
  },
  "8897415": {
    limit: 0,
    name: "Shona"
  },
  "2873940": {
    limit: 1,
    name: "Cassandra Herbert"
  },
  "2281739": {
    limit: 1,
    name: "Jenny Caws"
  },
  "3842908": {
    limit: 1,
    name: "Samantha Wills"
  }
}