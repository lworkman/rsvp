export const addToDatabase = (value) => {
  const db = window["firebase"].firestore();
  db.settings({ timestampsInSnapshots: true })

  const id = Math.floor(Math.random() * 564727123).toString();

  return db.collection("Responses").doc(id).set({ id, ...value });
}